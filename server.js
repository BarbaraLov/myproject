const express = require ('express')
const axios = require ('axios');
const fs = require('fs');
const path = require('path');

const app = express();
const PORT = 3000;

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'www'));

app.use(express.static(path.join(__dirname, "www")));


const dataFilePath = './objekt.JSON';


app.get('/', (req,res) => {
    fs.readFile(dataFilePath, 'utf8', (error, data) => {
        if(error) {
            res.status(500).send('Serverfehler');
        }else {
            const salonData = JSON.parse(data);
            const salons = salonData.salons;
            const treatments = salonData.treatments;
            res.render('index', {salons, treatments})
            console.log(salons)
            console.log(treatments)
        }
    }) 
})

app.post('/find', async(req, res) => {
    const searchItem = await salons.treatments.name
    console.log(searchItem)

    res.send(`We can search : ${searchItem}`)
})


app.listen(PORT, () => {
    console.log(`Server is running on ${PORT}` )
})